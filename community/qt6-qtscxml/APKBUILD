# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtscxml
pkgver=6.4.3
pkgrel=0
pkgdesc="Static and runtime integration of SCXML models into Qt6 code"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtscxml-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtscxml-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0d6b31b1c5f29f19fb15ee5a77e100c5df122f1ccba6229131456f4d66ee99978b0392a0f3c61ae1e31923041a973e80dc23df7814ed704a4d80d75b60741fac  qtscxml-everywhere-src-6.4.3.tar.xz
"

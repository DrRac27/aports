# Contributor: Lauren N. Liberda <lauren@selfisekai.rocks>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=rocketchat-desktop
pkgver=3.8.17
pkgrel=0
pkgdesc="Official Desktop Client for Rocket.Chat"
url="https://github.com/RocketChat/Rocket.Chat.Electron"
arch="aarch64 x86_64"	# electron
license="MIT"
depends="electron"
makedepends="
	electron-dev
	electron-tasje
	nodejs
	npm
	vips-dev
	yarn
"
options="net !check"	# dev dependencies purged before check
source="
	https://github.com/RocketChat/Rocket.Chat.Electron/archive/refs/tags/$pkgver/rocketchat-desktop-$pkgver.tar.gz
	tasje-package-json.patch

	rocketchat-desktop
"
builddir="$srcdir/Rocket.Chat.Electron-$pkgver"

export ELECTRON_OVERRIDE_DIST_PATH=/usr/bin

prepare() {
	default_prepare

	yarn install --ignore-scripts --frozen-lockfile
}

build() {
	NODE_ENV=production \
	NODE_OPTIONS=--openssl-legacy-provider \
	yarn build

	yarn install --ignore-scripts --frozen-lockfile --production

	npm rebuild sharp --nodedir=/usr/include/electron/node_headers --build-from-source

	tasje pack --config electron-builder.json
}

package() {
	install -Dm644 tasje_out/resources/app.asar "$pkgdir"/usr/lib/$pkgname/app.asar

	install -Dm755 "$srcdir"/$pkgname "$pkgdir"/usr/bin/$pkgname

	install -Dm644 tasje_out/rocketchat.desktop "$pkgdir"/usr/share/applications/rocketchat.desktop
	while read -r size; do
		install -Dm644 tasje_out/icons/$size.png "$pkgdir"/usr/share/icons/hicolor/$size/apps/$pkgname.png
	done < tasje_out/icons/size-list
}

sha512sums="
39028dba6c5d591d22df0b2b07ff76af9101dea74844b3fb75de397b6133698d641506f9c9a5082b38806df5f7988af0841c77cc3f808887271f88eba4d9b13f  rocketchat-desktop-3.8.17.tar.gz
bacb147316df48c0fb1685efde885077c4faced0c94f12f9c49f8a5306ee0ac9d2198719f4566f5612d54d70f61e560f3ead33f9a31cd0ce786ce0dbe1da267c  tasje-package-json.patch
268648ff0cf707ab67d5ad2e8291308f6815ff185e84f85455eebfda7ede228333e405705ecb743fe4a0b5e88f73aab46866aba5c7316f319161199427199695  rocketchat-desktop
"
